<?php
namespace emilasp\angular\assets\ui;

use yii\web\View;

/**
 * AngularUIAsset
 *
 * Class AngularUiUploaderAsset
 * @package emilasp\angular\assets\ui
 */
class AngularUiUploaderAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/angular-ui-uploader';

    /**
     * @inheritdoc
     */
    public $js = [
        'dist/uploader.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'emilasp\angular\assets\AngularAsset'
    ];

    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
}
