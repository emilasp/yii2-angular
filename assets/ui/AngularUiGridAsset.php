<?php
namespace emilasp\angular\assets\ui;

use yii\web\View;

/**
 * AngularUIAsset
 *
 * Class AngularUIAsset
 * @package emilasp\angular\assets\ui
 */
class AngularUiGridAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/angular-ui-grid';

    /**
     * @inheritdoc
     */
    public $js = [
        'ui-grid.js',
    ];
    /**
     * @inheritdoc
     */
    public $css = [
        'ui-grid.css',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'emilasp\angular\assets\AngularAsset'
    ];

    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
}
