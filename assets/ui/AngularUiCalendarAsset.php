<?php
namespace emilasp\angular\assets\ui;

use yii\web\View;

/**
 * AngularUIAsset
 *
 * Class AngularUiCalendarAsset
 * @package emilasp\angular\assets\ui
 */
class AngularUiCalendarAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/angular-ui-calendar';

    /**
     * @inheritdoc
     */
    public $js = [
        'src/calendar.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'emilasp\angular\assets\AngularAsset'
    ];

    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
}
