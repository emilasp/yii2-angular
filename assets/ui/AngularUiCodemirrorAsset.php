<?php
namespace emilasp\angular\assets\ui;

use yii\web\View;

/**
 * AngularUIAsset
 *
 * Class AngularUiCodemirrorAsset
 * @package emilasp\angular\assets\ui
 */
class AngularUiCodemirrorAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/angular-ui-codemirror';

    /**
     * @inheritdoc
     */
    public $js = [
        'ui-codemirror.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'emilasp\angular\assets\AngularAsset',
        'emilasp\angular\assets\vendor\CodemirrorAsset'
    ];

    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
}
