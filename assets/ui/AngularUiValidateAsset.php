<?php
namespace emilasp\angular\assets\ui;

use yii\web\View;

/**
 * AngularUIAsset
 *
 * Class AngularUiValidateAsset
 * @package emilasp\angular\assets\ui
 */
class AngularUiValidateAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/angular-ui-validate';

    /**
     * @inheritdoc
     */
    public $js = [
        'dist/validate.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'emilasp\angular\assets\AngularAsset'
    ];

    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
}
