<?php
namespace emilasp\angular\assets\ui;

use yii\web\View;

/**
 * AngularUIAsset
 *
 * Class AngularUiEventAsset
 * @package emilasp\angular\assets\ui
 */
class AngularUiEventAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/angular-ui-event';

    /**
     * @inheritdoc
     */
    public $js = [
        'dist/event.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'emilasp\angular\assets\AngularAsset'
    ];

    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
}
