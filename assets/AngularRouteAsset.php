<?php
namespace emilasp\angular\assets;

use yii\web\View;

/**
 * AngularRouteAsset
 *
 * Class AngularRouteAsset
 * @package emilasp\angular\assets
 */
class AngularRouteAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/angular-route';

    /**
     * @inheritdoc
     */
    public $js = [
        'angular-route.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'emilasp\angular\assets\AngularAsset'
    ];

    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
}
