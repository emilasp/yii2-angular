<?php
namespace emilasp\angular\assets;

use yii\web\View;

/**
 * AngularValidationAsset
 *
 * Class AngularValidationAsset
 * @package emilasp\angular\assets
 */
class AngularValidationAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/angular-validation/dist';

    /**
     * @inheritdoc
     */
    public $js = [
        'angular-validation.js',
        'angular-validation-rule.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'emilasp\angular\assets\AngularAsset'
    ];

    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
}
