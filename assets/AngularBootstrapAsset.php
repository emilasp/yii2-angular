<?php
namespace emilasp\angular\assets;

use yii\web\View;

/**
 * AngularBootstrapAsset
 *
 * Class AngularBootstrapAsset
 * @package emilasp\angular\assets
 */
class AngularBootstrapAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/angular-bootstrap';

    /**
     * @inheritdoc
     */
    public $js = [
        'ui-bootstrap-tpls.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'emilasp\angular\assets\AngularAsset'
    ];

    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if (class_exists('yii\bootstrap\BootstrapAsset')) {
            array_push($this->depends, 'yii\bootstrap\BootstrapAsset');
        } else {
            array_push($this->depends, 'emilasp\angular\assets\BootstrapAsset');
        }
    }
}
