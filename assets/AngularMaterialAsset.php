<?php
namespace emilasp\angular\assets;

use yii\web\View;

/**
 * AngularMaterialAsset
 *
 * Class AngularMaterialAsset
 * @package emilasp\angular\assets
 */
class AngularMaterialAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/angular-material';

    /**
     * @inheritdoc
     */
    public $js = [
        'angular-material.js',
    ];
    /**
     * @inheritdoc
     */
    public $css = [
        'angular-material.min.css',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'emilasp\angular\assets\AngularAsset'
    ];


    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
}
