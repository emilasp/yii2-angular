<?php
namespace emilasp\angular\assets;

use yii\web\View;

/**
 * AngularTouchAsset
 *
 * Class AngularTouchAsset
 * @package emilasp\angular\assets
 */
class AngularTouchAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/angular-touch';

    /**
     * @inheritdoc
     */
    public $js = [
        'angular-touch.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'emilasp\angular\assets\AngularAsset'
    ];

    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
}
