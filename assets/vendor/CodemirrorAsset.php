<?php
namespace emilasp\angular\assets\vendor;

use yii\web\View;


/**
 * CodemirrorAsset
 *
 * Class CodemirrorAsset
 * @package emilasp\angular\assets\vendor

 */
class CodemirrorAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = __DIR__ . '/assets/CodeMirror-master';

    /**
     * @inheritdoc
     */
    public $js = [
        'src/codemirror.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'emilasp\angular\assets\AngularAsset'
    ];

    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
}
