<?php
namespace emilasp\angular\assets;

use yii\web\View;


/**
 * AngularAnimateAsset
 *
 * Class AngularAnimateAsset
 * @package emilasp\angular\assets
 */
class AngularAnimateAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/angular-animate';

    /**
     * @inheritdoc
     */
    public $js = [
        'angular-animate.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'emilasp\angular\assets\AngularAsset'
    ];

    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
}
