<?php
namespace emilasp\angular\assets;

use yii\web\View;

/**
 * AngularResourceAsset
 *
 * Class AngularResourceAsset
 * @package emilasp\angular\assets
 */
class AngularResourceAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/angular-resource';

    /**
     * @inheritdoc
     */
    public $js = [
        'angular-resource.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'emilasp\angular\assets\AngularAsset'
    ];

    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
}
