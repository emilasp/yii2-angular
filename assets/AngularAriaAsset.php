<?php
namespace emilasp\angular\assets;

use yii\web\View;

/**
 * AngularAriaAsset
 *
 * Class AngularAriaAsset
 * @package emilasp\angular\assets
 */
class AngularAriaAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/angular-aria';

    /**
     * @inheritdoc
     */
    public $js = [
        'angular-aria.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'emilasp\angular\assets\AngularAsset'
    ];

    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
}
