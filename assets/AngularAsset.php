<?php
namespace emilasp\angular\assets;

use yii\web\View;

/**
 * AngularAsset
 *
 * Class AngularAsset
 * @package emilasp\angular\assets
 */
class AngularAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/angular';

    /**
     * @inheritdoc
     */
    public $js = [
        'angular.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\web\JqueryAsset',
    ];

    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
}
