<?php
namespace emilasp\angular\assets;

use yii\web\View;

/**
 * AngularMessagesAsset
 *
 * Class AngularMessagesAsset
 * @package emilasp\angular\assets
 */
class AngularMessagesAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/angular-messages';

    /**
     * @inheritdoc
     */
    public $js = [
        'angular-messages.min.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'emilasp\angular\assets\AngularAsset'
    ];


    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
}
