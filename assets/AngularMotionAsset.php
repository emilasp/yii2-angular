<?php
namespace emilasp\angular\assets;

use yii\web\View;

/**
 * AngularMotionAsset
 *
 * Class AngularMotionAsset
 * @package emilasp\angular\assets
 */
class AngularMotionAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/angular-motion';

    /**
     * @inheritdoc
     */
    public $js = [
        'dist/angular-motion.js',
    ];
    /**
     * @inheritdoc
     */
    public $css = [
        'dist/angular-motion.min.css',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'emilasp\angular\assets\AngularAsset'
    ];


    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
}
