<?php
namespace emilasp\angular\components\base;

use yii\web\AssetBundle;
use DirectoryIterator;
use yii;

/**
 * Class AngularAsset
 * @package emilasp\angular\components\base
 */
class AngularAsset extends AssetBundle
{
    public $css = [
        'css/site.css',
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'emilasp\angular\assets\AngularAsset',
        'emilasp\angular\assets\AngularRouteAsset',
        'emilasp\angular\assets\AngularResourceAsset',
        'emilasp\angular\assets\AngularAriaAsset',
        'emilasp\angular\assets\AngularAnimateAsset',
        'emilasp\angular\assets\AngularMaterialAsset',
        'emilasp\angular\assets\AngularMessagesAsset',
        'emilasp\angular\assets\AngularTouchAsset',
        'emilasp\angular\assets\AngularValidationAsset',

        'emilasp\angular\assets\ui\AngularUiCalendarAsset',
        //'emilasp\angular\assets\ui\AngularUiCodemirrorAsset',
        'emilasp\angular\assets\ui\AngularUiEventAsset',
        'emilasp\angular\assets\ui\AngularUiGridAsset',
        'emilasp\angular\assets\ui\AngularUiUploaderAsset',
        'emilasp\angular\assets\ui\AngularUiValidateAsset',


    ];


    /** @var array */
    private $assets = [
        'services'    => ['exts' => ['js']],
        'filters'     => ['exts' => ['js']],
        'directives'  => ['exts' => ['js']],
        'controllers' => ['exts' => ['js']],
        'modules'     => ['exts' => ['js']],
    ];
    /** @var array */
    public $assetsFiles = [];

    private $module;


    public function init()
    {
        $this->module     = Yii::$app->controller->module;
        $this->sourcePath = $this->module->angularPath;

        parent::init();

        $this->setCustomAppOptions();
        $this->setAngularAssetFiles();
        $this->setModuleAssetBundle();
    }

    /**
     * Добавляем из PHP в JS настройки приложения
     */
    private function setCustomAppOptions()
    {
        $view      = Yii::$app->controller->view;
        $assetUrl  = $view->assetManager->getPublishedUrl($this->sourcePath);
        $apiDomain = Yii::$app->controller->module->apiDomain;

        $js        = <<<JS
        window.document.angularAssetPath = '{$assetUrl}';
        window.document.apiDomain = '{$apiDomain}';
JS;
        $view->registerJs($js, yii\web\View::POS_HEAD);
    }

    /**
     * Собираем все файлы фронта для формироавния AssetBundle модуля
     */
    private function setAngularAssetFiles()
    {
        $this->assetsFiles['app'] = ['app.js'];

        foreach ($this->assets as $folder => $params) {
            $exts                       = $params['exts'];
            $this->assetsFiles[$folder] = $this->getFiles($folder, $exts);
        }
    }

    /** Обходим папку и отдаём найденные файлы
     * @param string $folder
     * @param  array $exts
     * @return array
     */
    private function getFiles($folder, $exts = ['js'])
    {
        $files = [];
        $path  = $this->module->angularPath . DIRECTORY_SEPARATOR . $folder;
        if (is_dir($path)) {
            foreach (new DirectoryIterator($path) as $file) {
                if ($file->isDot() || !in_array($file->getExtension(), $exts)) {
                    continue;
                }
                if (is_file($path . DIRECTORY_SEPARATOR . $file->getFilename())) {
                    $files[] = $folder . DIRECTORY_SEPARATOR . $file->getFilename();
                }

            }
        }
        return $files;
    }

    /** Формируем AssetBundle
     */
    private function setModuleAssetBundle()
    {
        $files = [];
        foreach ($this->assetsFiles as $folder => $folderFiles) {
            foreach ($folderFiles as $file) {
                $files[] = $file;
            }
        }

        $this->sourcePath = $this->module->angularPath;
        $this->js         = $files;
        $this->css        = ['assets/css/app.css'];
    }
}
