<?php
namespace emilasp\angular\components\base;

use emilasp\core\CoreModule;

/**
 * Class AngularModule
 * @package emilasp\angular
 */
abstract class AngularModule extends CoreModule
{
    public $modulePath;
    public $angularPath;

    public $apiDomain = '';

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->setFrontModulePaths();
        $this->setModuleLayout();
    }

    /**
     * Устанавливаем пути для фронт модуля. По сути это все файлы angular части модуля.
     */
    private function setFrontModulePaths()
    {
        $class_info = new \ReflectionClass($this);

        $this->modulePath  = dirname($class_info->getFileName());
        $this->angularPath = $this->modulePath . DIRECTORY_SEPARATOR . 'angular' . DIRECTORY_SEPARATOR;
    }

    /**
     * Собираем все файлы фронта для формироавния AssetBundle модуля
     */
    private function setModuleLayout()
    {
        $layoutPath = $this->angularPath . '/views/layouts/';
        if (is_dir($layoutPath)) {
            $this->layoutPath = $layoutPath;
            $this->layout = 'layout';
        }
    }
}
